import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Login from './pages/Login'
import Navigator from './pages/Navigator'
import Dashboard from './pages/Dashboard'
import Core from './pages/Core'

export default class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/navigator" component={Navigator} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/core" component={Core} />
          <Route render={() => <center><h1>Page not found</h1></center>} /> 
        </Switch>
      </Router>
    );
  }
}
