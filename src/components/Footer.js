import React, { Component } from 'react'
import { withStyles,Typography, } from '@material-ui/core'

const styles = theme => ({
  footer:{
    position:'absolute',
    bottom:'0px',
    left:'40%'
  }
});

class Footer extends Component {
  render() {
    const { classes } = this.props;
    return (
        <React.Fragment>
            <Typography className={classes.footer} variant="body1">
              &copy; 2019 HealthPlan Services. All Rights Reserved
            </Typography>
        </React.Fragment>
    );
  }
}

export default withStyles(styles)(Footer);