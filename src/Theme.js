import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
    overrides:{    
        MuiCssBaseline:{
            '@global': {
                body: {
                    backgroundColor:'#144c73'
                }
            }            
        },
        MuiTypography:{
            h1:{

            },
            h2:{

            },
            h3:{

            },
            h4:{

            },
            h5:{

            },
            h6:{
                color: '#ffffff',
            },
            subtitle1:{

            },
            body1:{
                fontSize: '0.875rem',
                fontWeight: '400',
                lineHeight: '1.5',
                letterSpacing: '0.01071em',
                color: '#ffffff',
                margin:'5px auto',
            }
        },
        MuiAppBar:{
            root:{
                height:60,
                backgroundColor:'#144c73 !important'
            }            
        }
    }
})

export default theme;
