import React from 'react'
import PropTypes from 'prop-types'
import { Button,FormControl,
  FormControlLabel,Checkbox,
  Input,InputLabel,
  Paper,Typography } from '@material-ui/core'
import withStyles from '@material-ui/core/styles/withStyles'
import img from '../assets/Logo_horizontal_web.png'



const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block',
    margin:'20vh auto',
    height:'50vh',
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop:'2vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  imageWrapper:{
    margin:'5px auto'
  }
});

const handleClick = () => {
  window.location.href="/navigator"
}

function Login(props) {
  const { classes } = props;

 
  return (
    <main className={classes.main}>
     <center className={classes.imageWrapper}>
        <img src={img} align="middle" alt='web-logo' />
     </center>
      <Paper className={classes.paper}>
        <Typography variant="h4">
          Configurator
        </Typography>
        <form className={classes.form}>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">What's Your Username?</InputLabel>
            <Input id="email" name="email" autoComplete="email" autoFocus />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">What's Your Password?</InputLabel>
            <Input name="password" type="password" id="password" autoComplete="current-password" />
          </FormControl>
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleClick}
            className={classes.submit}
          >
            Login
          </Button>
        </form>
      </Paper>
      <Typography align='center' variant="body1">
          &copy; 2019 HealthPlan Services. All Rights Reserved
      </Typography>
    </main>
  );
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);