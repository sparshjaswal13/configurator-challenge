import React, { Component } from 'react'
import { Button } from '@material-ui/core'
import Header from '../components/Header'
import Footer from '../components/Footer'

export default class Dashboard extends Component {
  render() {

    return (
        <React.Fragment>
          <Header />
          <center>
          <Button 
            style={{width:'50px',margin:'20px auto'}} 
            variant="contained"
            onClick={()=>{window.location.href='core'}} 
            color="default" >
            NEW
          </Button>
          </center>
          <Footer />
        </React.Fragment>
    );
  }
}
