import React, { Component } from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import { Button , withStyles, Typography } from '@material-ui/core'
import uploadImg from '../assets/upload.png'
import signImg from '../assets/sign-out.png'
import metadataImg from '../assets/metadata.png'
import dashboardImg from '../assets/dashboard.png'
import adminImg from '../assets/admin.png'
import fileImg from '../assets/file.png'

const styles = theme => ({
  button: {
    width:"30%",
    margin:'5px',
    minWidth:'150px',
    minHeight:'50px'
  },
  container:{
    width:'60%',
    height:'20vh',
    margin:'10%  auto'
  }

});

class Navigator extends Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Header />
        <div className={classes.container}> 
          <Button variant="contained" color="default" 
            onClick={()=> { window.location.href='/dashboard'}}  
            className={classes.button}>
          <div>
            <img src={dashboardImg} width="100px" alt="dashboard" />
            <span>
              <Typography variant='body2'>
                DashBoard
              </Typography>
            </span> 
          </div> 
        </Button>
          <Button variant="contained" color="default" className={classes.button}>
          <div>
            <img src={metadataImg} width="100px" alt="dashboard" />
            <span>
              <Typography variant='body2'>
                Manage Metadata
              </Typography>
            </span> 
          </div> 
        </Button>
          <Button variant="contained" color="default" className={classes.button}>
          <div>
            <img src={fileImg} width="100px" alt="dashboard" />
            <span>
              <Typography variant='body2'>
                Review & Approve
              </Typography>
            </span> 
          </div> 
        </Button>
        </div>  
        <div className={classes.container} >
          <Button variant="contained" color="default" className={classes.button}>
          <div>
            <img src={uploadImg} width="100px" alt="dashboard" />
            <span>
              <Typography variant='body2'>
                Publish
              </Typography>
            </span> 
          </div> 
        </Button>
          <Button variant="contained" color="default" className={classes.button}>
          <div>
            <img src={adminImg} width="100px" alt="dashboard" />
            <span>
              <Typography variant='body2'>
                System Administrator
              </Typography>
            </span> 
          </div> 
        </Button>
          <Button variant="contained" color="default" className={classes.button}>
          <div>
            <img src={signImg} width="100px" alt="dashboard" />
            <span>
              <Typography variant='body2'>
                Sign Out
              </Typography>
            </span> 
          </div> 
        </Button>
        </div>        
        <Footer />  
      </React.Fragment>
    );
  }
}

export default withStyles(styles) (Navigator)