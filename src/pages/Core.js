import React, { Component } from 'react'
import { withStyles} from '@material-ui/core'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Drawer from '../components/Drawer'

export default class Core extends Component {
  render() {

    return (
        <React.Fragment>
          <Header />
          <Drawer />
          <Footer />
        </React.Fragment>
    );
  }
}
