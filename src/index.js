import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import Theme from './Theme'
import {MuiThemeProvider} from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

ReactDOM.render(
    <MuiThemeProvider theme={Theme}>
        <App/>
        <CssBaseline/>
        
    </MuiThemeProvider>, document.getElementById('root'));

